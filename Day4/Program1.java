/*
 Code 1: Max Consecutive Ones

Company: Google, Facebook, Amazon, Microsoft, Apple, Uber, Airbnb, Adobe, Goldman Sachs,
Bloomberg
Platform: Leetcode - 485, Coding Ninja
Striver’s SDE Sheet
Description:
Given a binary array nums, return the maximum number of consecutive 1's in the array.
Example 1:
Input: nums = [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s. The
maximum number of consecutive 1s is 3.
Example 2:
Input: nums = [1,0,1,1,0,1]
Output: 2

Constraints:
1 <= nums.length <= 105
nums[i] is either 0 or 1.
*/

import java.util.*;

class ArrayDemo {

	int countOnes(int arr[]) {

		int cnt1 = 0;
		int maxcnt = 0;
		for(int i=0;i<arr.length;i++) {
		
			//for(int j=i+1;j<arr.length;j++) {

				if(arr[i] == 1){

					cnt1 ++;
					if(maxcnt < cnt1)
						maxcnt = cnt1;
				}
				else 
					cnt1 = 0;
			//}
		}
		return maxcnt;
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		ArrayDemo obj = new ArrayDemo();
		System.out.println("Enter size of array :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter array elements :");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		int retVal = obj.countOnes(arr);
		System.out.println("Consecutive Ones count : " + retVal);
	}
}

