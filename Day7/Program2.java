/* 2.Wave Array */

import java.util.*;
class ArrayDemo {


	void swap(int arr[],int a,int b) {

		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}
	
	void waveArr(int arr[]) {

		Arrays.sort(arr);

		for(int i=0;i<arr.length-1;i++) {

			swap(arr,i,i+1);
		}
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter size:");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements :");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		obj.waveArr(arr);
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i] + " ");
		}
	}
}

