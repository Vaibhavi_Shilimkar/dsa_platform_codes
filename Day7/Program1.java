/* Check if Array is sorted and rotated*/


import java.util.*;

class ArrayDemo {

	boolean checkSortedRotated(int arr[]) {

		int x = 0;
		int y = 0;

		for(int i=0;i<arr.length-1;i++) {

			if(arr[i] < arr[i+1]) 
				x++;
			else 
				y++;
		}

		if(y == 1 || y == 0) {

			if(arr[arr.length-1] < arr[0])
				x++;
			else 
				y++;

			if(y == 1)
				return true;
		}

		return false;
	}

}

class Client {

	public static void main(String[] args) {

		int arr[] = {1,2,3};

		ArrayDemo obj = new ArrayDemo();

		boolean x = obj.checkSortedRotated(arr);
	       	
		if(x) 
			System.out.println("true");
		else 
			System.out.println("false");
	}
}	
		
			
