/*3.Convert array into zig zag fashion */

import java.util.*;
class ArrayDemo {

	void zigZag(int arr[]) {

		Arrays.sort(arr);

		int temp = 0;
		boolean flag = true;

		for(int i=0;i<arr.length-1;i++) {

			if(flag) {
			
				if(arr[i] > arr[i+1]) {
					
					temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = temp;
				}
			}
			else {
				if(arr[i] < arr[i+1]) {

					temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = temp;
				}
			}
			flag = !flag;
		}
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		obj.zigZag(arr);
		System.out.println(Arrays.toString(arr));
	}
}
