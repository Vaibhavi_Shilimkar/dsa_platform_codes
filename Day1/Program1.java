/*
 Easy Level
 Code 1:Single Number
Company : Amazon, wipro, Capgemini, DXC technology, Schlumberger,
Avizva, epam, cadence, paytm, atlassian,cultfit+7
Platform: LeetCode - 136
Striver’s SDE Sheet
Description:
Given a non-empty array of integers nums, every element appears
twice except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use
only constant extra space.
Example 1:
Input: nums = [2,2,1]
Output: 1
Example 2:
Input: nums = [4,1,2,1,2]
Output: 4
Example 3:
Input: nums = [1]
Output: 1
Constraints:
1 <= nums.length <= 3 * 10^4
-3 * 104 <= nums[i] <= 3 * 10^4
Each element in the array appears twice except for one element
which appears only once.
*/

import java.util.*;

class ArrayDemo {

	int uniqueElement(int arr[]) {

		int flag = 0;
		if(arr.length == 1) {
			return arr[0];
		}
		else {

			for(int i=0;i<arr.length;i++) {
				int cnt = 0;
				for(int j=0;j<arr.length;j++) {
					if(arr[i] == arr[j]) {
						cnt ++;
					}
				}
				if(cnt == 1) {
					flag = 1;
					return arr[i];
				}
			}
		}
		return -1;
		
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size :");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter elements:");
		for(int i=0;i<arr.length;i++) {
			arr[i] = sc.nextInt();
		}
		ArrayDemo obj = new ArrayDemo();
		int retVal = obj.uniqueElement(arr);
		System.out.println("Unique Elment :" + retVal);
	}

}

