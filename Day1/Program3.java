/*
Code3: Key Pair
Company: Zoho, Flipkart, Morgan Stanley, Accolite, Amazon, Microsoft,
FactSet, Hike, Adobe, Google, Wipro, SAP Labs, CarWale
Platform: GFG
Description:
Given an array Arr of N positive integers and another number X. Determine
whether or not there exist two elements in Arr whose sum is exactly X.
Example 1:
Input:
N = 6, X = 16
Arr[] = {1, 4, 45, 6, 10, 8}
Output: Yes
Explanation: Arr[3] + Arr[4] = 6 + 10 = 16
Example 2:
Input:
N = 5, X = 10
Arr[] = {1, 2, 4, 3, 6}
Output: Yes
Explanation: Arr[2] + Arr[4] = 4 + 6 = 10

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N ≤ 105
1 ≤ Arr[i] ≤ 105
*/

import java.util.*;
class ArrayDemo {

	boolean sumOfElement(int arr[],int sum) {

		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++) {

				if(arr[i] + arr[j] == sum)
				       return true;
			}
		}
		return false;
	}	
}
class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements :");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}
		System.out.println("Enter sum:");
		int sum = sc.nextInt();

		ArrayDemo obj = new ArrayDemo();
		boolean retVal = obj.sumOfElement(arr,sum);
		if(retVal)
			System.out.println("Yess");
		else
			System.out.println("No");

	}
}
