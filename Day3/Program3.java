//
/*
Code3 : Count pairs with given sum
Company: Amazon, MakeMyTri, Facebook, UnitedHealth Group
Platform: GFG
Love Babbar’s SDE Sheet
Description:
Given an array of N integers, and an integer K, find the number of pairs of
elements in the array whose sum is equal to K.
Example 1:

Input:
N = 4, K = 6
arr[] = {1, 5, 7, 1}
Output: 2
Explanation:
arr[0] + arr[1] = 1 + 5 = 6 and arr[1] + arr[3] = 5 + 1 = 6.
Example 2:
Input:
N = 4, K = 2
arr[] = {1, 1, 1, 1}
Output: 6
Explanation:
Each 1 will produce sum 2 with any 1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 105
1 <= K <= 108
1 <= Arr[i] <= 106
*/

import java.util.*;

class ArrayDemo {
	int countSum(int arr[],int val) {
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			for(int j = i + 1 ; j < arr.length ; j++) {
				if(arr[i] + arr[j] == val) {
					count++;
				}
			}
		}
		return count;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the elements at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.print("Enter the val : ");
		int val = sc.nextInt();

		int ret = obj.countSum(arr,val);
		System.out.println("Count : " + ret);
	}
}

