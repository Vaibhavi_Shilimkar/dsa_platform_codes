/*
 Code2 : Equal Left and Right Subarray Sum
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n positive numbers. The task is to find the first index in the
array such that the sum of elements before it equals the sum of elements after it.
Note: Array is 1-based indexed.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: For second test case at position 3 elements before it (1+3) =
elements after it (2+2).

Example 2:
Input:

n = 1
A[] = {1}
Output: 1
Explanation: Since it's the only element hence it is the only point.
Expected Time Complexity: O(N)
Expected Space Complexity: O(1)
Constraints:
1 <= n <= 106
1 <= A[i] <= 108
*/

import java.util.*;

class ArrayDemo {

	void findElement(int arr[]) {
		
		for(int i=0;i<arr.length;i++) {
			int sum1 = 0;
			int sum2 = 0;

			for(int j=0;j<i;j++) {
				sum1 = sum1 + arr[j];
			}
			for(int k=i+1;k<arr.length;k++) {

				sum2 = sum2 + arr[k];
			}

			if(sum1 == sum2) { 
				System.out.println("Index :" + (i+1));
				break;
			}
		}
	}	

}

class Client {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements:");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		obj.findElement(arr);
	}
}

