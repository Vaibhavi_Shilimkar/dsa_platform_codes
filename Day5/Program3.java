/*
 Code3 : Equilibrium Point
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n non negative numbers. The task is to find the first
equilibrium point in an array. Equilibrium point in an array is an index (or position) such
that the sum of all elements before that index is the same as the sum of elements after
it.
Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: The equilibrium point is at position 3 as the sum of elements
before it (1+3) = sum of elements after it (2+2).
Example 2:
Input:
n = 1
A[] = {1}
Output: 1
Explanation: Since there's only an element hence its only the equilibrium point.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
0 <= A[i] <= 109
*/

import java.util.*;

class ArrayDemo {

	void equilibrium(int arr[]) {

		for(int i=0;i<arr.length;i++) {

			int sum1 = 0;
			int sum2 = 0;

			for(int j=0;j<i;j++) {

				sum1 = sum1 + arr[j];
			}
			for(int k=i+1;k<arr.length;k++) {

				sum2 = sum2 + arr[k];
			}

			if(sum1 == sum2) {
				
				System.out.println("Index : " + (i+1));
				break;
			}
		}
	}		       	
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements :");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		obj.equilibrium(arr);
	}
}
