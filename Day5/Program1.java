/*
 Code 1: Find Pivot Index
Company: Amazon, Adobe
Platform: Leetcode - 724
Given an array of integers nums, calculate the pivot index of this array.
The pivot index is the index where the sum of all the numbers strictly to the left of the
index is equal to the sum of all the numbers strictly to the index's right.
If the index is on the left edge of the array, then the left sum is 0 because there are no
elements to the left. This also applies to the right edge of the array.
Return the leftmost pivot index. If no such index exists, return -1.

Example 1:
Input: nums = [1,7,3,6,5,6]
Output: 3
Explanation:
The pivot index is 3.
Left sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11
Right sum = nums[4] + nums[5] = 5 + 6 = 11
Example 2:
Input: nums = [1,2,3]
Output: -1
Explanation:
There is no index that satisfies the conditions in the problem statement.
Example 3:
Input: nums = [2,1,-1]
Output: 0
*/

import java.util.*;

class ArrayDemo {

	void findPivot(int arr[]) {

		int flag = 0;
		for(int i=0;i<arr.length;i++) {

			int sum1 = 0;
			int sum2 = 0;

			for(int j=0;j<i;j++) {

				sum1 = sum1 + arr[j];
			}
			for(int k=i+1;k<arr.length;k++) {

				sum2 = sum2 + arr[k];
			}

			if(sum1 == sum2) {
				System.out.println("Pivot Index" + i);
				flag = 1;
				break;
			}
		}
		if(flag == 0)
			System.out.println("-1");
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter array size:");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter array elements :");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		obj.findPivot(arr);
	}
}



