/*Code 1: Merge Sorted Array
Company: Amazon, Samsung, LinkedIn +50 companies
Platform: Leetcode - 88
Fraz’s and Striver’s SDE sheet
Description:
You are given two integer arrays nums1 and nums2, sorted in non-decreasing order,
and two integers m and n, representing the number of elements in nums1 and nums2
respectively.
Merge nums1 and nums2 into a single array sorted in non-decreasing order.
The final sorted array should not be returned by the function, but instead be stored
inside the array nums1. To accommodate this, nums1 has a length of m + n, where the
first m elements denote the elements that should be merged, and the last n elements
are set to 0 and should be ignored. nums2 has a length of n.
Example 1:
Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
Output: [1,2,2,3,5,6]
Explanation: The arrays we are merging are [1,2,3] and [2,5,6].
The result of the merge is [1,2,2,3,5,6] with the underlined elements coming from
nums1.
Example 2:
Input: nums1 = [1], m = 1, nums2 = [], n = 0
Output: [1]
Explanation: The arrays we are merging are [1] and [].
The result of the merge is [1].
Example 3:
Input: nums1 = [0], m = 0, nums2 = [1], n = 1
Output: [1]
Explanation: The arrays we are merging are [] and [1].
The result of the merge is [1].
Note that because m = 0, there are no elements in nums1. The 0 is only there to ensure
the merge result can fit in nums1.

Constraints:
nums1.length == m + n
nums2.length == n
0 <= m, n <= 200
1 <= m + n <= 200
-109 <= nums1[i], nums2[j] <= 109
*/

import java.util.*;

class ArrayDemo {

	void merge(int arr1[],int arr2[],int arr3[],int n1, int n2) {

		int i = 0,j = 0,k = 0;
	       	while(i<n1 && j<n2) {

			if(arr1[i] == 0) {
				i++;
				continue;
			}
			else if(arr2[j] == 0) {
				j++;
				continue;
			}
			else if(arr1[i] < arr2[j]) {
				arr3[k] = arr1[i];
				i++;
			}
			else {
				arr3[k] = arr2[j];
				j++;
			}
			k++;
		}
		while(i<n1) {
			arr3[k] = arr1[i];
			i++;
			k++;
		}
		while(j<n2) {
			arr3[k] = arr2[j];
			j++;
			k++;
		}	
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter Arr1 size");
		int size1 = sc.nextInt();
		System.out.println("Enter Arr2 size");
		int size2 = sc.nextInt();

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];
		int arr3[] = new int[size1 + size2];

		System.out.println("Enter Arr1 elements:");
		for(int i=0;i<size1;i++) {
			arr1[i] = sc.nextInt();
		}
		System.out.println("Enter Arr2 elements :");
		for(int i=0;i<arr2.length;i++) {

			arr2[i] = sc.nextInt();
		}
		obj.merge(arr1,arr2,arr3,size1,size2);

		for(int i=0;i<arr3.length;i++) {

			System.out.print(arr3[i] + " ");
		}
	}
}
