/*
Code 2: Longest Common prefix in a Array
Company: VMWare, Microsoft, Google
Platform: leetcode-14

Description:
Write a function to find the longest common prefix string amongst an array of
strings.
If there is no common prefix, return an empty string "".
Example 1:
Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:
Input: strs = ["dog", "racecar", "car"]
Output: ""
Explanation: There is no common prefix among the input strings.

Constraints:
1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lowercase English letters.
*/

import java.util.*;
class StringDemo {

	String longPrefix(String arr[]) {

		int n = arr.length;
		String result = arr[0];

		for(int i=0;i<arr.length;i++) {

			while(arr[i].indexOf(result) != 0) {

				result = result.substring(0,result.length()- 1);

				if(result.isEmpty())
					return "-1";
			}
		}

		return result;
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		StringDemo obj = new StringDemo();

		System.out.println("Enter no. of String length :");
		int len = sc.nextInt();

		String[] arr = new String[len];
	
		System.out.println("Enter String");
		
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.next();
		}
	
		String retVal = obj.longPrefix(arr);
		System.out.println(retVal);
	}
}
