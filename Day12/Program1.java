
/*
Code1: Move Zeros

Company: Paytm, Amazon, Microsoft, Samsung, SAP Labs, Linkedin,
Bloomberg, BlueStack
Platform:Leetcode-283, GFG
Fraz’s and Striver’s SDE Sheet
Description:
Given an integer array nums, move all 0's to the end of it while maintaining
the relative order of the non-zero elements.
Note that you must do this in-place without making a copy of the array.
Example 1:
Input: nums = [0,1,0,3,12]
Output: [1,3,12,0,0]
Example 2:
Input: nums = [0]
Output: [0]
Constraints:
1 <= nums.length <= 104
-231 <= nums[i] <= 231 - 1
*/

import java.util.*;

class Solution {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the elemen at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.println("Array before sorting : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		// Logic
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			if(arr[i] != 0) {
				arr[count] = arr[i];
				count++;
			}
		}

		while(count < arr.length) {
			arr[count] = 0;
			count++;
		}

		System.out.println("Array after sorting : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}

