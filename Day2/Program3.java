/*Code3 : Find the smallest and second smallest element in
an array
Company: Amazon, Goldman Sachs
Platform: GFG
Description:
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.

Example 2:
Input :
6
1 2 1 3 6 7
Output :
1 2
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1<=N<=105
1<=A[i]<=105*/

import java.util.*;
class ArrayDemo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements:");
		
		for(int i=0;i<arr.length;i++) {
			arr[i] = sc.nextInt();
		}

		int min = Integer.MAX_VALUE;
		int store = Integer.MAX_VALUE;;

		for(int i=0;i<arr.length;i++) {
			
				
			if(arr[i] < min) {

				min = arr[i];
			}
		}

		for(int i=0;i<arr.length;i++) {
			if(arr[i] < store && arr[i] > min) {

				store = arr[i];
			}
		}

		System.out.println("Smallest element : " + min);
		System.out.println("Second Smallest element : " + store);

	}
}

