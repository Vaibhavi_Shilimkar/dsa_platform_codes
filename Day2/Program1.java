/*Code 1: Two Sum

Company: Google, Meta, Amazon, Microsoft, Paypal+76 more companies
Platform : Leetcode - 1
Fraz’s & striver’s SDE sheet.
Description
Given an array of integers nums and an integer target, return indices of the two
numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use
the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]

Constraints:
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.*/

import java.util.*;
class TwoSum {

	void sumRange(int arr[],int sum) {

		int flag = 0;
		for(int i=0;i<arr.length-1;i++) {
			int add = 0;
			for(int j=i+1;j<arr.length;j++) {

				add = add + arr[i] + arr[j];
				if(add == sum) {
					flag = 1;
					System.out.println(i + " " + j);
				}
			}
			if(flag == 1) 
				break;
		}
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements :");
		for(int i=0;i<arr.length;i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter sum :");
		int sum = sc.nextInt();

		TwoSum obj = new TwoSum();
		obj.sumRange(arr,sum);
		}
}

