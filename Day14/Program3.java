
/*
Code 3: Smallest subarray with sum greater than x
Company: Accolite, Amazon, Goldman Sachs, Google, Facebook

Platform: GFG
Given an array of integers (A[]) and a number x, find the smallest subarray with
sum greater than the given value. If such a subarray do not exist return 0 in that
case.
Example 1:
Input:
A[] = {1, 4, 45, 6, 0, 19}
x = 51
Output: 3
Explanation:
Minimum length subarray is
{4, 45, 6}

Example 2:
Input:
A[] = {1, 10, 5, 2, 7}
x = 9
Output: 1
Explanation:
Minimum length subarray is {10}

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N, x ≤ 105
0 ≤ A[] ≤ 104
*/

import java.util.*;

class Solution {
	int minimumLengthSubArray(int arr[], int k) {
		for(int i = 0 ; i < arr.length ; i++) {
			if(arr[i] > k) {
				return 1;
			}
			int sum = 0;
			int count = 0;
			for(int j = i + 1 ; j < arr.length ; j++) {
				sum = sum + arr[j];
				count++;
				if(sum > k) {
					return count;
				}
			}
		}
		return 0;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		Solution obj = new Solution();

		System.out.print("Enter the sum : ");
		int k = sc.nextInt();

		int ret = obj.minimumLengthSubArray(arr,k);
		System.out.println("Minimum length subarray with the given sum is : " + ret);
	}
}
