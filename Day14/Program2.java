
/*
 Code 2: Length Unsorted Subarray
Company: Flipkart, Microsoft, Adobe, Make my trip
Platform: GFG
Given an unsorted array Arr of size N. Find the subarray Arr[s...e] such that
sorting this subarray makes the whole array sorted.

Example 1:
Input:
N = 11
Arr[] ={10,12,20,30,25,40,32,31,35,50,60}
Output: 3 8
Explanation: Subarray starting from index
3 and ending at index 8 is required
subarray. Initial array: 10 12 20 30 25
40 32 31 35 50 60 Final array: 10 12 20
25 30 31 32 35 40 50 60
(After sorting the bold part)
Example 2:
Input:
N = 9
Arr[] = {0, 1, 15, 25, 6, 7, 30, 40, 50}
Output: 2 5
Explanation: Subarray starting from index
2 and ending at index 5 is required
Subarray.
Initial array: 0 1 15 25 6 7 30 40 50
Final array: 0 1 6 7 15 25 30 40 50
(After sorting the bold part)
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 107
1 ≤ Arr[i] ≤ 108
*/

import java.util.*;

class Solution {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int start = 0;
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			start = i;
			for(int j = i + 1 ; j < arr.length ; j++) {
				if(arr[i] > arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
					count++;
				}
			}
		}
		int end = start + count;
		System.out.println(start + " " + end);
	}
}
