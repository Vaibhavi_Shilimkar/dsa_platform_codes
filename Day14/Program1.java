
/*
 Code 1: Intersection of Two Arrays
Company: Accolite, Amazon, Microsoft, PayPal, Rockstand
Platform: Leetcode - 349
Striver’s SDE sheet
Given two integer arrays nums1 and nums2, return an array of their intersection.
Each element in the result must be unique and you may return the result in any
order.
Example 1:
Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]
Example 2:
Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [9,4]
Explanation: [4,9] is also accepted.
Constraints:
1 <= nums1.length, nums2.length <= 1000
0 <= nums1[i], nums2[i] <= 1000
*/

import java.util.HashSet;
import java.util.Scanner;
import java.util.Arrays;

class IntersectionExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input for nums1
        System.out.print("Enter the size of nums1: ");
        int size1 = scanner.nextInt();
        int[] nums1 = new int[size1];
        System.out.println("Enter elements for nums1:");
        for (int i = 0; i < size1; i++) {
            nums1[i] = scanner.nextInt();
        }

        // Input for nums2
        System.out.print("Enter the size of nums2: ");
        int size2 = scanner.nextInt();
        int[] nums2 = new int[size2];
        System.out.println("Enter elements for nums2:");
        for (int i = 0; i < size2; i++) {
            nums2[i] = scanner.nextInt();
        }

        int[] intersection = findIntersection(nums1, nums2);

        System.out.println("Intersection: " + Arrays.toString(intersection));
    }

    public static int[] findIntersection(int[] nums1, int[] nums2) {
        // Use sets to store unique elements
        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> intersectionSet = new HashSet<>();

        // Add elements from nums1 to set1
        for (int num : nums1) {
            set1.add(num);
        }

        // Check for intersection with nums2 and add to intersectionSet
        for (int num : nums2) {
            if (set1.contains(num)) {
                intersectionSet.add(num);
            }
        }

        // Convert the intersection set to an array
        int[] intersection = new int[intersectionSet.size()];
        int index = 0;
        for (int num : intersectionSet) {
            intersection[index++] = num;
        }

        return intersection;
    }
}
